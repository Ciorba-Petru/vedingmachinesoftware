import org.apache.logging.log4j.*;

import java.io.*;
import java.text.*;
import java.util.*;

public class VendingMachine {
    Logger logger = LogManager.getLogger(VendingMachine.class);
    Scanner scanner = new Scanner(System.in);
    String userProductCode;
    List<String> productsCode = new ArrayList<>();
    JSONRead jsonRead = new JSONRead();
    Map<String, String> productsAndIdentifier = new LinkedHashMap<>();

    public void printVedingMachineMenu() throws IOException {
        String str = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Map<Integer, Character> productsCodeForMenu = new HashMap<>();
        productsCodeForMenu.put(0, ' ');
        for (int i = 0; i <= 25; i++) {
            productsCodeForMenu.put(i, str.charAt(i));
        }

        int rows = jsonRead.jsonConfigAndItems.getConfig().getRows();
        int columns = jsonRead.jsonConfigAndItems.getConfig().getColumns();
        logger.debug("The machine menu has been configured");
        System.out.println("*********************");
        System.out.println(" Veding Machine Menu");
        System.out.println("       " + "Hello!!!");
        System.out.println("*********************");
        for (int i = 0; i <= rows; i++) {
            System.out.print(productsCodeForMenu.get(i) + " ");
            for (int j = 1; j <= columns; j++) {

                if (i == 0) {
                    System.out.print(" " + j);
                    System.out.print(" ");
                } else {
                    productsCode.add("" + productsCodeForMenu.get(i) + j);
                    System.out.print("" + productsCodeForMenu.get(i) + j + " ");
                }
            }
            System.out.println();
        }
        List<String> products = new ArrayList<>();
        List<String> price = new ArrayList<>();

        for (JSONItems item : jsonRead.jsonConfigAndItems.getItems()) {
            products.add(item.getName());
        }
        for (JSONItems item : jsonRead.jsonConfigAndItems.getItems()) {
            price.add(item.getPrice());
        }
        for (int i = 0; i < products.size(); i++) {
            productsAndIdentifier.put(productsCode.get(i), products.get(i) + ", " + price.get(i));
        }
        for (Map.Entry<String, String> entry : productsAndIdentifier.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
    }

    boolean checkIfProductCodeIsIncorrect() throws IOException, ParseException {
        if (productsAndIdentifier.containsKey(userProductCode)) {
            logger.info("You choose: " + productsAndIdentifier.get(userProductCode));
            return false;
        } else
            logger.warn("You write wrong row or column name or this product is missing. Enter again: ");

        return true;
    }

    public void clientMachineInteraction() throws IOException, ParseException {
        printVedingMachineMenu();
        System.out.println();

        do {
            logger.info("Please, choose the row and column of the selected product: ");
            logger.debug(userProductCode = scanner.nextLine());
        } while (checkIfProductCodeIsIncorrect());

        logger.info("Please indicates amount of this product: ");
        int clientProductAmount = scanner.nextInt();
        CalculatorIMPL calculatorIMPL = new CalculatorIMPL();
        calculatorIMPL.amountCalc(clientProductAmount, userProductCode, productsCode);
        String forPayment = calculatorIMPL.totalPriceCalc(clientProductAmount, userProductCode, productsCode);
        logger.debug("The total price of product was calculated");
        NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
        Number number = format.parse(forPayment);
        double payment = number.doubleValue();

        logger.info("For payment you need: " + "$" + payment);
        logger.info("You want to did payment? Enter: Ok / Cancel");
        scanner.nextLine();
        String clientAnswerForPayment = scanner.nextLine();
        if (clientAnswerForPayment.equalsIgnoreCase("Ok")) {
            Payment paymentState = new Payment();
            paymentState.setProductCode(userProductCode);
            paymentState.setPrice(payment);
            logger.info(paymentState);
            logger.info("Enter the payment amount in $: ");
            double clientCashPayment = scanner.nextDouble();
            paymentState.collectCash(clientCashPayment);
            logger.info(paymentState);
            paymentState.cancelTransaction();
            logger.info(paymentState);
            paymentState.dispenseItem();
            logger.info("Thanks for the purchase, good luck!!!");
            scanner.nextLine();
        } else {
            logger.debug("User choose cancel option. The process will started from the beginning");
            clientMachineInteraction();
        }

    }
}
