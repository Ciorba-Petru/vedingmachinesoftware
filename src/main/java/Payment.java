import org.apache.logging.log4j.*;

public class Payment {
    private PaymentState ready;
    private PaymentState dispenseChange;
    private PaymentState dispenseItem;
    private PaymentState transactionCancelled;
    private PaymentState state;
    private double collectedCash;
    private String productCode;
    private double price;
    Logger logger = LogManager.getLogger(VendingMachine.class);

    public Payment() {
        ready = new ReadyState(this);
        dispenseChange = new DispenseChangeState(this);
        dispenseItem = new DispenseItemState(this);
        transactionCancelled = new TransactionCancelledState(this);
        state = ready;
    }

    public void collectCash(double amount) {
        this.collectedCash += amount;
        logger.info("The paymenet was receive");
        state.collectCash();
    }

    public Payment setCollectedCash(double collectedCash) {
        this.collectedCash = collectedCash;
        return this;
    }

    public PaymentState getState() {
        return state;
    }

    public void setState(PaymentState state) {
        this.state = state;
    }

    public void dispenseChange(String productCode) {
        this.state.dispenseChange(productCode);
    }

    public void cancelTransaction() {
        this.state.cancelTransaction();
    }

    public double calculateChange(String productCode) {
        return collectedCash - getPrice();
    }

    public void dispenseItem() {
        this.state.dispenseItem();
    }

    public double getCollectedCash() {
        return collectedCash;
    }

    public PaymentState getReady() {
        return ready;
    }

    public void setReady(PaymentState ready) {
        this.ready = ready;
    }

    public PaymentState getDispenseChange() {
        return dispenseChange;
    }

    public void setDispenseChange(PaymentState dispenseChange) {
        this.dispenseChange = dispenseChange;
    }

    public PaymentState getDispenseItem() {
        return dispenseItem;
    }

    public void setDispenseItem(PaymentState dispenseItem) {
        this.dispenseItem = dispenseItem;
    }

    public PaymentState getTransactionCancelled() {
        return transactionCancelled;
    }

    public void setTransactionCancelled(PaymentState transactionCancelled) {
        this.transactionCancelled = transactionCancelled;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        String str = "Current state of machine: " + state;
        return str;
    }
}