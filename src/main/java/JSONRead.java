import com.fasterxml.jackson.databind.*;
import org.apache.logging.log4j.*;

import java.io.*;

public class JSONRead {
    JSONConfigAndItems jsonConfigAndItems;
    Logger logger = LogManager.getLogger(JSONRead.class);
    ObjectMapper objectMapper = new ObjectMapper();
    File dir = new File("D:/json/");
    String jsonFile;

    {
        try {
            File[] files = dir.listFiles((d, name) -> name.endsWith(".json"));
            for (int i = 0; i < files.length; i++) {
                jsonFile = String.valueOf(files[i]);
            }
            jsonConfigAndItems = objectMapper.readValue(new File(jsonFile), JSONConfigAndItems.class);
            logger.debug("The connection with the .json file has been established");
        } catch (IOException exception) {
            logger.error("No file found", exception);
        }
    }
}
