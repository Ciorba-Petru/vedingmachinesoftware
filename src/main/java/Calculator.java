import java.io.*;
import java.text.*;
import java.util.*;

public interface Calculator {
    boolean amountCalc(int userAmount, String userProduceCoordinates, List<String> allProducesCoordinates) throws IOException, ParseException;

    String totalPriceCalc(int userAmount, String userProduceCoordinates, List<String> allProducesCoordinates);

}
