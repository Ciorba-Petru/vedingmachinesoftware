import org.apache.logging.log4j.*;

public class ReadyState implements PaymentState {
    private Payment payment;
    Logger logger = LogManager.getLogger(ReadyState.class);

    public ReadyState(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void collectCash() {
        if (payment.getCollectedCash() > payment.getPrice()) {
            payment.setState(payment.getDispenseChange());
        } else {
            logger.info("You have entered less money than the price of the product");
            payment.setState(payment.getTransactionCancelled());

        }

    }

    @Override
    public void dispenseChange(String productCode) {
        this.payment.setState(new DispenseChangeState(this.payment));
        this.payment.dispenseChange(productCode);
    }

    @Override
    public void dispenseItem() {
        System.out.println("Start from begin");
    }

    @Override
    public void cancelTransaction() {
        this.payment.setState(new TransactionCancelledState(payment));
        this.payment.cancelTransaction();
    }

    @Override
    public String toString() {
        return "Ready to receive payment";
    }
}
