
import org.apache.logging.log4j.*;

import java.io.*;
import java.text.*;

public class MainClass {
    public static void main(String[] args) throws IOException, ParseException {
        Logger logger = LogManager.getLogger(MainClass.class);
        boolean run = true;
        VendingMachine vedingMachine = new VendingMachine();
        while (run) {
            logger.trace("Application start from main class");
            vedingMachine.clientMachineInteraction();
        }
    }

}
