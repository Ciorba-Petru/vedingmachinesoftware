import java.util.*;

public class JSONConfigAndItems {
    JSONConfig config;
    private List<JSONItems> items;

    public JSONConfigAndItems() {

    }

    public JSONConfigAndItems(JSONConfig config, List<JSONItems> items) {
        this.config = config;
        this.items = items;
    }

    public JSONConfig getConfig() {
        return config;
    }

    public void setConfig(JSONConfig config) {
        this.config = config;
    }

    public List<JSONItems> getItems() {
        return items;
    }

    public void setItems(List<JSONItems> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return " items=" + items;
    }
}
