import org.apache.logging.log4j.*;

public class DispenseChangeState implements PaymentState {
    private Payment payment;
    Logger logger = LogManager.getLogger(DispenseChangeState.class);

    public DispenseChangeState(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void collectCash() {
        throw new RuntimeException("Dispensing Change. Unable to collect cash");
    }

    @Override
    public void dispenseChange(String productCode) {

        double change = this.payment.calculateChange(productCode);
        if (change == 0) {
            logger.info("No change to return");
        } else {
            logger.info("Change of " + String.format("%.2f", change) + " returned");
        }
        payment.setState(payment.getDispenseItem());
    }

    @Override
    public void dispenseItem() {
        logger.error("Dispensing Change. Unable to dispense Item");
    }

    @Override
    public void cancelTransaction() {
        dispenseChange(payment.getProductCode());
    }

    @Override
    public String toString() {
        String str = "Dispensing calculate";
        return str;
    }
}
