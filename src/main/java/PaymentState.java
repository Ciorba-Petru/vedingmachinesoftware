public interface PaymentState {
    public void collectCash();

    public void dispenseChange(String productCode);

    public void dispenseItem();

    public void cancelTransaction();
}