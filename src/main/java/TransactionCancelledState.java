import org.apache.logging.log4j.*;

public class TransactionCancelledState implements PaymentState {
    private Payment payment;
    Logger logger = LogManager.getLogger(TransactionCancelledState.class);

    public TransactionCancelledState(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void collectCash() {
        logger.error("Unable to collect cash in a cancelled transaction");
    }

    @Override
    public void dispenseChange(String productCode) {
        logger.error("Unable to dispense change in a cancelled transaction");
    }

    @Override
    public void dispenseItem() {
        logger.error("Unable to dispense item in a cancelled transaction");
    }

    @Override
    public void cancelTransaction() {
        logger.info("Returning " + "$" + payment.getCollectedCash());
        payment.setCollectedCash(0);
        payment.setState(new ReadyState(payment));
    }

    @Override
    public String toString() {
        return "Transaction Cancelled";
    }
}
