import org.apache.logging.log4j.*;

import java.io.*;
import java.text.*;
import java.util.*;

public class CalculatorIMPL implements Calculator {
    JSONRead jsonRead = new JSONRead();
    VendingMachine vedingMachine = new VendingMachine();

    @Override
    public boolean amountCalc(int userAmount, String userProducesCode, List<String> machineProducesCode) throws IOException, ParseException {
        Logger logger = LogManager.getLogger(CalculatorIMPL.class);
        Map<String, Integer> rowsColumnsKeyAndAmountValue = new LinkedHashMap<>();
        List<Integer> productsAmount = new ArrayList<>();
        for (JSONItems item : jsonRead.jsonConfigAndItems.getItems()) {
            productsAmount.add(item.getAmount());
        }
        for (int i = 0; i < productsAmount.size(); i++) {
            rowsColumnsKeyAndAmountValue.put(machineProducesCode.get(i), productsAmount.get(i));
        }
        int amount = rowsColumnsKeyAndAmountValue.get(userProducesCode);
        if (amount < userAmount) {
            logger.warn("Sorry, the quantity of this product is less then " + userAmount);
            vedingMachine.clientMachineInteraction();
            return true;
        } else if (userAmount == 0) {
            logger.warn("Sorry, you not select product");
            vedingMachine.clientMachineInteraction();
            return true;
        } else {
            //amount = -userAmount;
            //rowsColumnsKeyAndAmountValue.put(userProducesCode, amount);
            logger.info("This quantity is avalaible.");
            return false;
        }

    }

    @Override
    public String totalPriceCalc(int userAmount, String userProduceCoordinates, List<String> allProducesCoordinates) {
        Map<String, String> rowsColumnsKeyAndPriceValue = new LinkedHashMap<>();
        List<String> productsPrice = new ArrayList<>();
        for (JSONItems item : jsonRead.jsonConfigAndItems.getItems()) {
            productsPrice.add(item.getPrice());
        }
        for (int i = 0; i < productsPrice.size(); i++) {
            rowsColumnsKeyAndPriceValue.put(allProducesCoordinates.get(i), productsPrice.get(i));
        }
        String price = rowsColumnsKeyAndPriceValue.get(userProduceCoordinates);
        String remove$ = price.replace('$', ' ');

        double finalPr = Double.parseDouble(remove$) * userAmount;

        String finalPrice = String.format("%.2f", finalPr);
        return finalPrice;
    }
}
