import org.apache.logging.log4j.*;

public class DispenseItemState implements PaymentState {
    private final Payment payment;
    Logger logger = LogManager.getLogger(DispenseChangeState.class);

    public DispenseItemState(Payment payment) {
        this.payment = payment;
    }

    @Override
    public void collectCash() {
        logger.error("Dispensing Change. Unable to collect cash");
    }

    @Override
    public void dispenseChange(String productCode) {
        logger.error("Dispensing Change. Unable to dispense change");
    }

    @Override
    public void dispenseItem() {
        logger.info("Dispensing item " + payment.getProductCode());
        payment.setState(new ReadyState(this.payment));
    }

    @Override
    public void cancelTransaction() {
        logger.error("Dispensing Item. Unable to cancel the transaction");
    }

    @Override
    public String toString() {
        return "Dispense Item";
    }
}
