# VendingMachineSoftware
This is a console application.


## Application run instructions
- First way:
1. Download code from [repository](git@gitlab.com:Ciorba-Petru/vedingmachinesoftware.git).
2. Create a folder `D:/json`, put in this folder a file with `.json` extension.
3. Open with an IDE like `IntelliJ IDEA`.
4. Run [MainClass](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/MainClass.java)
- Second way:
1. Download code from [repository](git@gitlab.com:Ciorba-Petru/vedingmachinesoftware.git).
2. Create a folder `D:/json`, put in this folder a file with `.json` extension.
3. Copy .jar file from `VedingMachineSoftware\out\artifacts\VedingMachineSoftware_jar` to desktop.
4. Open Windows Command Prompt Commands (CMD) on desktop and run the `.jar` file with command: `java -jar \way to VedingMachineSoftware.jar`



## Approaches took to solve the challenge.

Requirements:
- To add new products provide a `.json` file in a created folder `D:/json`in this format:
  ```{
  	"config": {
  		"rows": 4,
  		"columns": "8"
  	},
  	"items": [{
  		"name": "Snickers",
  		"amount": 10,
  		"price": "$1.35"
  	}, {
  		"name": "Hersheys",
  		"amount": 10,
  		"price": "$2.25"
  	}, {
  		"name": "Hersheys Almond",
  		"amount": 10,
  		"price": "$1.80"
  	}, {
  		"name": "Hersheys Special Dark",
  		"amount": 10,
  		"price": "$1.75"
  	}, {
  		"name": "Reese's",
  		"amount": 10,
  		"price": "$1.05"
  	}, {
  		"name": "Nutrageous",
  		"amount": 10,
  		"price": "$1.30"
  	}, {
  		"name": "Baby Ruth",
  		"amount": 10,
  		"price": "$2.50"
  	}, {
  		"name": "Milky Way",
  		"amount": 10,
  		"price": "$1.00"
  	}, {
  		"name": "M&M",
  		"amount": 10,
  		"price": "$1.25"
  	}]
  }
  ```
  `Jackson API` was used to read `.json` file. \
  And for manipulation with the .json file the following classes have been created: [JSONRead](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/JSONRead.java), [JSONConfigAndItems](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/JSONConfigAndItems.java), [JSONItems](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/JSONItems.java), [JSONConfig](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/JSONConfig.java)
- A [Vending Machine](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/VedingMachine.java) instance has been created in the [MainClass](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/MainClass.java), which prints the menu and makes all the application logic.
 - [CalculatorIMPL](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/CalculatorIMPL.java)  extends [Calculator](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/Calculator.java) interface and calculate products price and if exists products amount (without decreasing the quantity of the purchased product (this option is not reviewed in the code) )
- To `payment` implementation was used `State Design Pattern` which includes the following classes: [PaymentState](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/PaymentState.java), [Payment](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/Payment.java), [ReadyState](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/ReadyState.java), [DispenseItemState](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/DispenseItemState.java), [TransactionCancelledState](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/TransactionCancelledState.java), [DispenseChangeState](https://gitlab.com/Ciorba-Petru/vedingmachinesoftware/-/blob/main/src/main/java/DispenseChangeState.java),  

- For audit purposes was used `log4j2` API. The log file will be saved in the given path: `c:/log/VendingMachineSoftware.log"`



